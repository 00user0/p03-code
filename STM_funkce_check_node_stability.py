# -*- coding: utf-8 -*-
"""
Created on Thu Jul 27 11:19:19 2023

@author: matth
"""
import numpy as np
from STM_functions import *
from STM_function_solve import *

# verze cervenec 2023 - kontrluje pouze mnozstvi prutu a podpor, nikoliv stabilitu det(K)
"""
#%%

def check_node_stability_bar(abi, nodes, supports, bars, deleted_bar):
    
    # find bar in BARS -> [node 1, node 2] to detect 2 nodes of deleted bar
    checked_nodes = bars[deleted_bar]
    
    # find how many bars and supports are connected to node
    
    # list with bars index deleted for balance in nodes
    bdb = []
    
    for i in checked_nodes:
        
        # index connected s - supports, b - bars
        s = 0
        b = []
           
        # explore if active j-bar is connected to i-node
        for j in abi:
            if i in bars[j]:
                # add index of bar (connected to node)
                b.append(j)
                               
        # explore if j-support is connected to i-node            
        for j in supports[i]:
            if type(j) == int or type(j) == float:
                s += 1
        

        # check balance
        if s+len(b) < 3:
            bdb += b
            
            #print
            print("jedná se o uzel" + str(i))
            print(s,len(b))
            print("součet je: " + str(s+len(b)) )
            
            
    return bdb




def check_node_stability(nodes, supports, bars, new_deleted_bars, dbi, BARS_modul, E_del):
 
    # reshape support to 3 columns
    supports = supports.reshape(-1, 3)
    
    # create list of indexes of active bars - abi
    abi = []
    for i in range(len(bars)):
        if i not in dbi:
            abi.append(i)
    
    # list with bars index deleted for balance in nodes
    bdb = []
    
    # loop for 
    for i in new_deleted_bars:
        bdb_returned = check_node_stability_bar(abi, nodes, supports, bars, i)
        bdb += bdb_returned
        
    # update dbi list
    dbi += bdb
    
    for j in bdb:
        BARS_modul[j] = E_del
        
    return BARS_modul, dbi
"""            
#%% # verze spren 2023

def check_node_stability_bar(abi, nodes, supports, bars, deleted_bar):
    
    bdb = []
    #%% 3 
    ### Identifikace i koncových styčníků (KS) n-tého prutu
    
    # find bar in BARS -> [node 1, node 2] to detect 2 nodes of deleted bar
    checked_nodes = bars[deleted_bar]


    #%% 4     
    # find how many bars and supports are connected to node
    # i is index of node  
    for i in checked_nodes:

        # index connected s - supports, b - bars
        b = []
           
        # explore if active j-bar is connected to i-node
        for j in abi:
            if i in bars[j]:
                # add index of bar (connected to node)
                b.append(j)

        #################################################
        # 4a - find nodes of j-bars
        ## FS - fictitious construction
        ## FS_NODES = [central_node, node_1, node_2, ...]
        
        FS_NODES = []  
        FS_NODES.append(nodes[i])
        for j in b:
            for k in range(2):
  
                if bars[j][k] != i:
                    FS_NODES.append(nodes[bars[j][k]])
        
                    
    #%% 5
    ## Vytvoření fiktivní konstrukce, kde k-pruty mají mímo i-styčník tuhou podporu 
        FS_BARS = []
        for j in range(len(FS_NODES)-1):
            FS_BARS.append([0,j+1])            
        FS_BARS = np.array(FS_BARS)
        # NODES and BARS done
        
        # supports
        # generate vector u
        FS_SUPPORTS = []
        number = len(FS_NODES)*len(FS_NODES[0])
        varname = 'u'
        for j in range(number):
            exec(f"{varname}{i+1} = symbols('{varname}{i+1}')")
            FS_SUPPORTS.append([eval(f"{varname}{i+1}")])
        FS_SUPPORTS = np.array(FS_SUPPORTS)
        

        for j in range(3):
            FS_SUPPORTS[j][0] = supports[i*3+j][0]
        for j in range(len(FS_SUPPORTS)-3):
            FS_SUPPORTS[j+3][0] = 0
            
        # generate vector f
        # FS_f = np.array([])  
        # varname = 'f' 
        # for j in range(len(FS_SUPPORTS)):
        #     if type(FS_SUPPORTS[j]) == int or type(FS_SUPPORTS[j]) == float:
        #             exec(f"{varname}{j+1} = symbols('{varname}{j+1}')")
        #             exec(f"FS_f = np.r_[FS_f, {varname}{j+1}]")
        #     else:
        #         exec(f"{varname}{j+1} = symbols('{varname}{j+1}')")
        #         exec(f"{varname}{j+1} = float(0)") 
        #         exec(f"FS_f = np.r_[FS_f, {varname}{j+1}]")
        
        # generate vector f
        FS_f = copy.deepcopy(FS_SUPPORTS)
        for j in range(len(FS_f)):
            if type(FS_SUPPORTS[j][0]) == int or type(FS_SUPPORTS[j][0]) == float:
                FS_f[j][0] = 'f'
            else:
                FS_f[j][0] = 0

        FS_f = np.reshape(FS_f,[len(FS_f),1])
        FS_SUPPORTS = np.reshape(FS_SUPPORTS,[len(FS_SUPPORTS),1])
               
        E = 1
        A = 1
        

    #%% 6        
        stability = check_stability_of_node(FS_NODES, FS_BARS, FS_SUPPORTS, FS_f, E, A)
        if  type(stability) == str :
            print("něco se teď vymaže")
    #%% 7+8
            if len(b) == 2:
                A_B = []
                for m in range(2):
                    vx = FS_NODES[FS_BARS[m][1]][0]-FS_NODES[FS_BARS[m][0]][0]
                    vy =  FS_NODES[FS_BARS[m][1]][1]-FS_NODES[FS_BARS[m][0]][1]
                    vz = FS_NODES[FS_BARS[m][1]][2]-FS_NODES[FS_BARS[m][0]][2]
                    
                    A_B.append([vx,vy,vz])
                
                A = A_B[0]
                B = A_B[0]
                cross_product = np.cross(A, B)
                
                #8a
                if np.all(cross_product == 0):
                    # bars delete                   
                    bdb.append(b[1])
                    # bars connect two bars
                    bar_con = bars[b[0]].tolist() + bars[b[1]].tolist()
                    bar_con = [item for item in bar_con if item != 2]
                    bars[b[0]][0] = bar_con[0]
                    bars[b[0]][1] = bar_con[1]
                    
                    print("2 pruty byly spojeny")
                    fig = plt.figure()
                    plt.axis('equal')  
                    plot_2d(FS_NODES, FS_BARS, 'grey', '-',0.5, 'connected nodes')
                    plt.title("Checked nodes")
                    
                #8b
                else:
                    for l in b:
                        bdb.append(l)
            else:
                for l in b:
                    bdb.append(l)
        # else:
        #     print("nepovedeno")
    return bdb, bars, FS_NODES, FS_BARS, FS_SUPPORTS, FS_f

def check_node_stability(nodes, supports, bars, new_deleted_bars, dbi, BARS_modul, E_del):
    
    # create list of indexes of active bars - abi
    abi = []
    for i in range(len(bars)):
        if i not in dbi:
            abi.append(i)
    
    # list with bars index deleted for balance in nodes
    bdb = []
    
    # loop for 
    for k in new_deleted_bars:
        bdb_returned, bars, FS_NODES, FS_BARS, FS_SUPPORTS, FS_f = check_node_stability_bar(abi, nodes, supports, bars, k)
        bdb += bdb_returned
        
    # update dbi list
    dbi += bdb
    
    for j in bdb:
        BARS_modul[j] = E_del
    
    return BARS_modul, dbi, bars, FS_NODES, FS_BARS, FS_SUPPORTS, FS_f





#%% check deleting of bars

def check_deleting(F_del, number_odb, f_BARS, BARS_modul, dbi, NODES, BARS, u, f, BARS_area, C_bars_2):
    strange_iteration = True

    
    if number_odb == 0:
        BARS_modul,dbi, new_deleted_bars = delete_bars_2_3(F_del, f_BARS, BARS_modul, dbi)

    if number_odb >0:    
        BARS_modul,dbi, new_deleted_bars = delete_bars_2_2(number_odb, f_BARS, BARS_modul, dbi)    
    
    # check if det(K) = 0
    K_stability = check_stability_of_node(NODES, BARS, u, f, BARS_modul, BARS_area)
    if type(K_stability) == str:
        K_regular = False
    else:
        K_regular = True
    

    
    
    # check if last C - strain energy is strange
    solution = solve_truss(NODES, BARS, u, f, BARS_modul, BARS_area)
    if solution != None:
        f_BARS, BARS_pos, BARS_neg, BARS_no, f_s, u_s, u_BARS, u_pos, u_neg = solution
   
        if len(C_bars_2) < 3:
            C_ok = True
        else:
            
            str_energy = float(0.5*np.dot(f_BARS, u_BARS))           
            if str_energy > C_bars_2[-1]*5:
                C_ok = False
            else:
                C_ok = True
                print(round(str_energy,0), round(C_bars_2[-1],0))
    else:
        C_ok = False
    
    print(K_regular)
    print(C_ok)
    if K_regular == True and C_ok == True:
        strange_iteration = False
    
    return strange_iteration

def check_deleting_results(F_del, number_odb, f_BARS, BARS_modul, dbi, NODES, BARS, u, f, BARS_area):
    
    if number_odb == 0:
        BARS_modul,dbi, new_deleted_bars = delete_bars_2_3(F_del, f_BARS, BARS_modul, dbi)

    if number_odb >0:    
        BARS_modul,dbi, new_deleted_bars = delete_bars_2_2(number_odb, f_BARS, BARS_modul, dbi)    
        
    return BARS_modul,dbi, new_deleted_bars