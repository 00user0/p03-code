# -*- coding: utf-8 -*-
"""
Created on Fri Feb 24 18:18:33 2023

@author: matth
"""

import numpy as np
import sympy as sp
from sympy import symbols
import math
import matplotlib
import matplotlib.pyplot as plt
from math import sqrt, floor, ceil
import random
import copy

# ax = plt.axes(projection='3d')

#%% FUNCTIONS DEFINE NODES, ETC

# define functions
def plot_solid(matrix_base, matrix_apex, range_ax,c,lt,lw,lg):
  
    # points
    points = []
    len_mb = len(matrix_base)
    for j in matrix_base:
        points_row = []
        for k in range(3):
            points_row.append(j[k])
        points.append(points_row)
        
    for n in matrix_apex:
        points_row = []
        for k in range(3):
            points_row.append(n[k])
        points.append(points_row)    

    # bars
    bars = []

    for l in range(len(matrix_base)):
        # add opposite point
        bars.append([l,matrix_base[l][3]+len_mb])
        # make polygon in one plane
        if l<len(matrix_base)-1:
            bars.append([l,l+1])
        else:
            bars.append([l,0])
            
    for m in range(len(matrix_apex)):
        m = m + len_mb
        if len(matrix_apex) == 1:
            break
        if m-len_mb<len(matrix_apex)-1:
            bars.append([m,m+1])
        else:
            bars.append([m,len_mb])     

    points = np.array(points)     
    bars = np.array(bars)

    for i in range(len(bars)):
        xi, xf = points[bars[i,0],0], points[bars[i,1],0]
        yi, yf = points[bars[i,0],1], points[bars[i,1],1]
        zi, zf = points[bars[i,0],2], points[bars[i,1],2]
        line, = plt.plot([xi, xf],[yi, yf],[zi, zf],color=c,linestyle=lt, linewidth=lw)
        
        ax.set_xlim3d(0, range_ax)
        ax.set_ylim3d(0, range_ax)
        ax.set_zlim3d(0, range_ax)
    line.set_label(lg)
    plt.legend(prop={'size': 10})
    
    return points, bars


def generate_plane_coeff(points):
    # plane equation ax + bx + cx + d = 0
    # this function will generate coefficients a, b, c, d
    
    # create empty list with coefficients
        # row means one plane collumn is a, b, c ,d
        # 1. first row: base of shape
        # second row: apex of shape
        # next rows: faces of shape
    plane_coeff = []
        
    # 1. matrix_base
        # points 
    row = 0    
    A = points[row][0:3]
    row = 1
    B = points[row][0:3]
    row = 2
    C = points[row][0:3]
        # creating vectors

    AB = np.array(B) - np.array(A)
    AC = np.array(C) - np.array(A)

    # cross product
    R = np.cross(AB,AC).tolist()
    d = 0
    for d_i in range(3):
        d -= R[d_i]*A[d_i]
    R.append(d)
    plane_coeff.append(R)
    return plane_coeff
    
def generate_list_of_plane(matrix_base, matrix_apex):
    
        # 1. first row: base of shape
    points_in_plane = matrix_base[0:3]
    plane_coeff_i = generate_plane_coeff(points_in_plane)
    plane_coeff = []
    plane_coeff.append(plane_coeff_i[0])
    
        # 2. second row: apex of shape
    if len(matrix_apex)>2:
        points_in_plane = matrix_apex[0:3]
        plane_coeff_i = generate_plane_coeff(points_in_plane)
        plane_coeff.append(plane_coeff_i[0])
    else:
        plane_coeff.append([0,0,0,0])
    
        # next rows: faces of shape
        
    for i in range(len(matrix_base)):
        points_in_plane = []
        # add opposite point
        points_in_plane.append(matrix_base[i])
        # make polygon in one plane
        if i<len(matrix_base)-1:
            points_in_plane.append(matrix_base[i+1])
        else:
            points_in_plane.append(matrix_base[0])
        points_in_plane.append( matrix_apex[matrix_base[i][3]])
        
        plane_coeff_i = generate_plane_coeff(points_in_plane)
        plane_coeff.append(plane_coeff_i[0])
    
    return plane_coeff




def point_inside_shape(tested_point, point_inside, equations_planes):
    # this function takes points inside shape and on his borders
    # function return value True = points lies inside or on borders
    # solve signs of point related to a plane
    reference_sign = []
    for i in range(len(equations_planes)):
        row = equations_planes[i][3]
    
        for j in range(3):
            row += equations_planes[i][j]*point_inside[j]           
        reference_sign.append(row)
        
    # solve signs of tested point
        
    test_p_sign = []    
    for i in range(len(equations_planes)):
        row = equations_planes[i][3]
    
        for j in range(3):
            row += equations_planes[i][j]*tested_point[j]           
        test_p_sign.append(row)

    # check if tested point is inside   
    
    # test_p_sign[i] < 0:
    #   - (not) accept points on bordes - (<) <=
    # variable higher_or_lower
    #   - (not) accept points on bordes - (>) >=
    
    point_inside = True
    for i in range(len(test_p_sign)):
        if reference_sign[i] >= 0 and test_p_sign[i] < 0:
                point_inside = False
                break
        if reference_sign[i] <= 0 and test_p_sign[i] > 0:
                point_inside = False
                break
    return point_inside


def point_outside_shape(tested_point, point_inside, equations_planes):
    # this function takes points only inside shape
    # function return value True = points lies inside and not on borders
    # solve signs of point related to a plane
    reference_sign = []
    for i in range(len(equations_planes)):
        row = equations_planes[i][3]
    
        for j in range(3):
            row += equations_planes[i][j]*point_inside[j]           
        reference_sign.append(row)
        
    # solve signs of tested point
        
    test_p_sign = []    
    for i in range(len(equations_planes)):
        row = equations_planes[i][3]
    
        for j in range(3):
            row += equations_planes[i][j]*tested_point[j]
        test_p_sign.append(row)

    # check if tested point is inside   
    
    # test_p_sign[i] < 0:
    #   - (not) accept points on bordes - (<) <=
    # variable higher_or_lower
    #   - (not) accept points on bordes - (>) >=
    
    point_inside = True
    for i in range(len(test_p_sign)):
        if reference_sign[i] > 0 and test_p_sign[i] <= 0:
                point_inside = False
                break
        if reference_sign[i] < 0 and test_p_sign[i] >= 0:
                point_inside = False
                break
    return point_inside


# generating test cube of points
def generate_points_cube(xmin,xmax, disx):
    x_list = []
    for i in range(int(((xmax-xmin)/disx+1))):
        x_add_dist = round(disx*i,3) ## this row - round x value
        x_list.append(xmin+x_add_dist)
    return x_list

# generate matrix of points in cube
def connect_points(x,y,z):
    points = []
    for i in x:
        for j in y:
            for k in z:
                points.append([i,j,k])
    return points

# and plot
def plot_points(cube):
    cube_T = np.array(cube).T
    ax.scatter(cube_T[0], cube_T[1], cube_T[2],marker='o',s=10)




## test lies points inside shape?


# points_inside_planes, if boolean is True - add point if lies inside

def points_inside_planes(tested_points, one_p_in_sh, plane_coef, bolean_true_false, func_out_or_in):
    # variable func_out_or_in -  point_inside_shape, point_outside_shape
    points_inside_is = []
    for i in range(len(tested_points)):
        inside = func_out_or_in(tested_points[i], one_p_in_sh, plane_coef)
        if inside is bolean_true_false:
            points_inside_is.append(tested_points[i])
    return points_inside_is

#%% functions BARS
####### BARS

check_process = False

# bars generator
def generate_bars(nodes):
    if type(nodes) == list:
        nodes = np.array(nodes)
    BARS = np.empty((0,2))
    
    process_i_values = range(0,10000,500)
    
    for i in range(len(nodes)):   
        original = True
        for j in range(len(nodes-i)):
        # if there are two nodes with 2 of 3 same coordinates
            if i == j:
                break            
            if (nodes[i][0] == nodes[j][0]
                and nodes[i][1] != nodes[j][1]
                and nodes[i][2] == nodes[j][2]):
                if original == True:
                    BARS = np.append(BARS, np.array([[i,j]]), axis = 0)
                    original = False
                    
            else:
                BARS = np.append(BARS, np.array([[i,j]]), axis = 0)
                BARS = BARS.astype(int)
                
            # check process
            
            if check_process == True:
                print(len(BARS))       
                
    return BARS

# bars generator
#linear independence

def linear_indep(v1, v2):
    
    # vectors without zero
    vz1 = []
    vz2 = []
    
    # check if there are some components same zero 
    for i in range(3):
        if (v1[i] == 0 and v2[i] != 0) or (v2[i] == 0 and v1[i] != 0):
            return False
        if v1[i] != 0 and v2[i] != 0:
            vz1.append(v1[i])
            vz2.append(v2[i])
    ratio = []    
    for i in range(len(vz1)):
        a = vz1[i]/vz2[i]
        ratio.append(a)

    for i in range(len(ratio)):
        if ratio[0] == ratio[i]:
            pass
        else:
            return False
    return True

def generate_bars_0(nodes):
    if type(nodes) == list:
        nodes = np.array(nodes)
    BARS = np.empty((0,2))
    process_i_values = range(0,10000,500)
    
    # take i-node
    for i in range(len(nodes)):  
        # than take every node which had not been already connected
        j = i+1
        list_vector = []
        while j < len(nodes):
            
            # check if bar vector is unique
            vx1 = nodes[j][0]-nodes[i][0]
            vy1 =  nodes[j][1]-nodes[i][1]
            vz1 = nodes[j][2]-nodes[i][2]
            
            v1 = [vx1,vy1,vz1]
            
            # compare bar uniqueness with every
            unq = True
            for v2 in list_vector:
                if linear_indep(v1, v2):
                    unq = False
                    print(unq)
                    continue
            
            
            # unique bar
            if unq:
                list_vector.append(v1)
                BARS = np.append(BARS, np.array([[i,j]]), axis = 0)
                BARS = BARS.astype(int)
            #continue with iteration
            j += 1
    return BARS

"""
def generate_bars_3(nodes):
    if type(nodes) == list:
        nodes = np.array(nodes)
    BARS = np.empty((0, 2))

    process_i_values = range(0, 10000, 500)

    for i in range(len(nodes)):
        original = True
        for j in range(i + 1, len(nodes)):
            # Check if vectors are linearly dependent
            vec_i_j = nodes[j] - nodes[i]
            for k in range(i):
                vec_k_i = nodes[i] - nodes[k]
                if np.isclose(np.cross(vec_i_j, vec_k_i), 0).all():
                    # If vectors are linearly dependent, connect only with the closest node
                    dist_i_k = np.linalg.norm(vec_k_i)
                    dist_j_k = np.linalg.norm(nodes[j] - nodes[k])
                    if dist_i_k < dist_j_k:
                        BARS = np.append(BARS, np.array([[i, k]]), axis=0)
                    else:
                        BARS = np.append(BARS, np.array([[j, k]]), axis=0)
                    original = False
                    break
            if original and j != i:
                BARS = np.append(BARS, np.array([[i, j]]), axis=0)
                
            # Check process
            if j in process_i_values:
                print(j, len(BARS))

    return BARS.astype(int)


def generate_bars_2(nodes, check_process=False):
    if type(nodes) == list:
        nodes = np.array(nodes)
    BARS = np.empty((0,2), dtype=int)

    for i in range(len(nodes)):
        for j in range(i+1, len(nodes)):
            # Check if nodes have same vector as some other bar
            skip = False
            for k in range(len(BARS)):
                if (np.array_equal(nodes[i], nodes[BARS[k,0]]) and np.array_equal(nodes[j], nodes[BARS[k,1]])) or \
                   (np.array_equal(nodes[i], nodes[BARS[k,1]]) and np.array_equal(nodes[j], nodes[BARS[k,0]])):
                    skip = True
                    break
            if skip:
                continue
                
            # Check if nodes have two out of three same coordinates
            if nodes[i][0] == nodes[j][0] and \
               nodes[i][2] == nodes[j][2] and \
               nodes[i][1] != nodes[j][1]:
                # Add bar
                BARS = np.append(BARS, np.array([[i,j]], dtype=int), axis = 0)
            elif nodes[i][1] == nodes[j][1] and \
                 nodes[i][2] == nodes[j][2] and \
                 nodes[i][0] != nodes[j][0]:
                # Add bar
                BARS = np.append(BARS, np.array([[i,j]], dtype=int), axis = 0)
            elif nodes[i][0] == nodes[j][0] and \
                 nodes[i][1] == nodes[j][1] and \
                 nodes[i][2] != nodes[j][2]:
                # Add bar
                BARS = np.append(BARS, np.array([[i,j]], dtype=int), axis = 0)

    return BARS
"""
"""
def generate_all_bars(nodes):
    
    for i in range(len(nodes)):
        for j in range(len(nodes)):
            
            BARS = np.append(BARS, np.array([[i,j]]), axis = 0)
            BARS = BARS.astype(int)
    return BARS
"""


    
  
#%% DELETE BARS

def point_between_p1_p2(num, p1, p2):
    number_of_coord = 0 
    for i in range(len(p1)):
        lower = min(p1[i], p2[i])
        upper = max(p1[i], p2[i])
        if num[i] >= lower and num[i] <= upper:
            number_of_coord += 1
    if number_of_coord == 3:
        return True
    else:
        return False

def intersection_point(p1,p2, mp, planes_coeff):
    #######
    # this function return intersection points of plane δ and two points if exists
    #######
    
    intersection_points = []
    
    # vector v of line segment   
    vx = p2[0]-p1[0]
    vy = p2[1]-p1[1]
    vz = p2[2]-p1[2]
    
    # parametric equation of line
    t = symbols('t')
    px = p1[0] + vx*t
    py = p1[1] + vy*t
    pz = p1[2] + vz*t
    
    # explore every plane in list:
    for i in range(len(planes_coeff)):
        plane = planes_coeff[i] 
        
        # check if δ and v is orthogonal - vp = vector*normal_vector_plane
        vp = plane[0]*vx + plane[1]*vy + plane[2]*vz
        if vp == 0:
            continue
        
        else: # means non orthogonal
        # input parametric eq to δ
            # solve unknown t
            t_solve = sp.solveset(plane[0]*px + plane[1]*py + plane[2]*pz + plane[3],t)
            # convert t to float
            t_solve = list(t_solve)[0].evalf()
            
            # coordinates of point P
            Px = px.subs(t, t_solve).evalf()
            Py = py.subs(t, t_solve).evalf()
            Pz = pz.subs(t, t_solve).evalf()
            
            P = [Px, Py, Pz]
            
            
            # lies P on segment?
            if point_between_p1_p2(P, p1, p2) == True:
                # # lies P on shape?
                test = point_inside_shape(P,mp,planes_coeff)
                if test:
                    intersection_points.append(P)                
               
    return intersection_points
    
def generate_delete_bars(nodes, bars, mbd, mad, mpd):
    bars = bars.tolist()
    bars_new = []
    
    # strategy - add BAR to new list of BARS if ...
    planes_coeff = generate_list_of_plane(mbd, mad)
    
   
    for i in range(len(bars)):
        p1 = nodes[bars[i][1]]
        p2 = nodes[bars[i][0]]
        
        
        # mutual position of two nodes
        # 1)
        if not (point_outside_shape(p1, mpd, planes_coeff)  
            and point_outside_shape(p2, mpd, planes_coeff)
            and point_inside_shape(p1, mpd, planes_coeff)
            and point_inside_shape(p2, mpd, planes_coeff)):
            
            ip = intersection_point(p1,p2,mpd,planes_coeff)
            if len(ip) == 0:
                bars_new.append(bars[i])
                continue
            else:
                continue
            
        # 4) two nodes on plane
        #    if third point (intermediate point) is inside shape, delete bar
        if (point_outside_shape(p1, mpd, planes_coeff) == False 
            and point_outside_shape(p2, mpd, planes_coeff) == False
            and point_inside_shape(p1, mpd, planes_coeff)
            and point_inside_shape(p2, mpd, planes_coeff)): # if p1 and p2 lies on borders
            p3x = (p1[0]+p2[0])/2
            p3y = (p1[1]+p2[1])/2
            p3z = (p1[2]+p2[2])/2
            p3 = [p3x, p3y, p3z]
           
            # this function return false if point lies on borders or outside
            if point_outside_shape(p3, mpd, planes_coeff): #True value - p3 is inside shape
                # BARS should be deleted
                continue
            else:
                bars_new.append(bars[i])
                continue
        
        # 5) one node inside plane and second on border
        if (point_outside_shape(p1, mpd, planes_coeff)
            and point_outside_shape(p2, mpd, planes_coeff)
            ):
            continue
        
        # 6) two nodes inside plane
        if (point_outside_shape(p1, mpd, planes_coeff)
            and point_outside_shape(p2, mpd, planes_coeff)):
            continue
                
        # 2)-3)
        else:
            ip = intersection_point(p1,p2,mpd,planes_coeff)
            if len(ip) == 0:
                bars_new.append(bars[i])
                continue
            else:
                ip_orig = [] # original intersection points
                for j in range(len(ip)):
                    if j != p1 and j != p2:
                        ip_orig.append(j)
                
                if ip_orig > 0:
                    bars_new.append(bars[i])

    return np.array(bars_new)

    
    
def bar_inside_shape(tested_point, equations_planes):
    # this function takes points inside shape and on his borders
    # function return value True = points lies inside or on borders
    # solve signs of point related to a plane
    reference_sign = []
    for i in range(len(equations_planes)):
        row = equations_planes[i][3]
    
        for j in range(3):
            row += equations_planes[i][j]*tested_point[j]           
        reference_sign.append(row)
        
    # solve signs of tested point
        
    test_p_sign = []    
    for i in range(len(equations_planes)):
        row = equations_planes[i][3]
    
        for j in range(3):
            row += equations_planes[i][j]*tested_point[j]           
        test_p_sign.append(row)

    return test_p_sign
    

def p_with_del_apex(points, m_del_base, m_del_apex):
    
    # adjust m_del_base
    for j in m_del_base:
        del j[3] 
    # connect matrixes m_del   
    m_del = [*m_del_base, *m_del_apex]
    # same point is find = pif
    for i in range(len(m_del)):
        pif = False    
        for j in range(len(points)):
            while pif == False:
                if points[j] == m_del[i]:
                    pif == True                    
        if pif == False:
            points.append(m_del[i])
    return points 

# def bar_inside()


# plot bars
def plot_p_and_b(points, bars,c,lt,lw,lg):
    if type(points) == list:
        points = np.array(points)
    for i in range(len(bars)):
        xi, xf = points[bars[i,0],0], points[bars[i,1],0]
        yi, yf = points[bars[i,0],1], points[bars[i,1],1]
        zi, zf = points[bars[i,0],2], points[bars[i,1],2]
        line, = plt.plot([xi, xf],[yi, yf],[zi, zf],color=c,linestyle=lt, linewidth=lw)
        
    line.set_label(lg)
    plt.legend(prop={'size': 10})            

#%%


#%% LOAD, SUPPORTS

"""
# generate support

def support_area(matrix_shape):
    ma = matrix_shape[0]
    mb = matrix_shape[1]
    mp = matrix_shape[2]
    
"""
# generate load

def load_point(lp, nodes, load_vector):
    
    lp = np.array(lp)
    
    lp = lp.transpose()
    lpc = lp[0:3].transpose().tolist()
    lps = lp[3].transpose().tolist() # magnitude of F - units - N
    lpd = lp[4].transpose().tolist() # x(0), y(1), z(2)
    
    for j in range(len(lpc)):
        size = lps[j]
        direction = lpd[j]
        lpc_i = lpc[j]
        for i in range(len(nodes)):
            
            """
            print("")
            print(nodes[i])
            print(lpc_i)
            print("")
            """
            if nodes[i] == lpc_i:
                n_u = int(i*3 + direction)
                load_vector[n_u] = size
                continue

    return load_vector

"""
area_full_load = area_load*y_dim*(x_dim*2)
load_place = [x_dim_nodes, z_dim_nodes]

force_one_node = 0
force_nodes = np.empty((0,3))
for i in range(len(NODES)):
    if (abs(NODES[i][0]) == abs(load_place[0])
        and NODES[i][2] == load_place[1]
        and (type(f[i*3+2])) == float or type(f[i*3+2]) == int):
        force_one_node +=1
        force_nodes = np.append(force_nodes, np.array([NODES[i]]), axis =0)
for i in range(len(NODES)):
    if (abs(NODES[i][0]) == abs(load_place[0])
        and NODES[i][2] == load_place[1]
        and (type(f[i*3+2])) == float or type(f[i*3+2]) == int):
        f[i*3+2] = area_full_load/force_one_node
"""

#%% PLOT RESULTS

"""
def plot_2d(points, bars,c,lt,lw,lg):
        
    if type(points) == list:
        points = np.array(points)
    for i in range(len(bars)):
        xi, xf = points[bars[i,0],0], points[bars[i,1],0]
        zi, zf = points[bars[i,0],2], points[bars[i,1],2]


        line, = plt.plot([xi, xf],[zi, zf],color=c,linestyle=lt, linewidth=lw)          
    line.set_label(lg)
    plt.legend(prop={'size': 10})
"""
    

def plot_2d(points, bars,c,lt,lw,lg):
        
    if type(points) == list:
        points = np.array(points)
    for i in range(len(bars)):
        xi, xf = points[bars[i,0],0], points[bars[i,1],0]
        zi, zf = points[bars[i,0],2], points[bars[i,1],2]

        if type(lw) == int or type(lw) == float:
            line, = plt.plot([xi, xf],[zi, zf],color=c,linestyle=lt, linewidth=lw)

        if str(type(lw)) == "<class 'numpy.ndarray'>":
            scale = 4/max(abs(lw))
            width = bars[i][2]*scale
            line, = plt.plot([xi, xf],[zi, zf],color=c,linestyle=lt, linewidth=width)            
    line.set_label(lg)
    plt.legend(prop={'size': 10})
    
    
def plot_2d_1(points, bars,c,lt,lw,lg):
        
    if type(points) == list:
        points = np.array(points)
    for i in range(len(bars)):
        xi, xf = points[bars[i,0],0], points[bars[i,1],0]
        zi, zf = points[bars[i,0],2], points[bars[i,1],2]

        if type(lw) == int or type(lw) == float:
            line, = plt.plot([xi, xf],[zi, zf],color=c,linestyle=lt, linewidth=lw)

        if str(type(lw)) == "<class 'numpy.ndarray'>":
            scale = 4/max(abs(lw))
            width = bars[i][2]*scale
            line, = plt.plot([xi, xf],[zi, zf],color=c,linestyle=lt, linewidth=width)            
    # Add text annotations for each bar
    for i in range(len(bars)):
        i_idx, j_idx = bars[i, 0], bars[i, 1]
        text_pos = ((points[i_idx][0] + points[j_idx][0]) / 2,
                    (points[i_idx][2] + points[j_idx][2]) / 2)
        if abs(bars[i][2]) > max(abs(lw)/1000):
            plt.text(*text_pos, str(bars[i][2]), fontsize=5, color='black')

