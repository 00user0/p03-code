# -*- coding: utf-8 -*-
"""
Created on Sat Nov  4 15:21:38 2023

@author: matth
"""

# -*- coding: utf-8 -*-
"""
Created on Fri Feb 24 18:03:14 2023

@author: matth
"""

from STM_functions import *
from STM_function_solve import *
import dill


#%% MODEL



z = 1

# Shape 1
mb = [[0,0,0,0],
      [3,0,0,1],
      [3,0,z,2],
      [0,0,z,3]]

ma = [[0,0.01,0],
      [3,0.01,0],
      [3,0.01,z],
      [0,0.01,z]]


mp = [0.1,0.0005,0.1]

# shape delete


# load
# lp - load point [point_x, point_y, point_z, value, direction]

lp = [[1,0,1,-1500000,2],
      [2,0,1,-1500000,2]]

# lp = [[1.5,0,0,-960000,2]]

# support
sp = [[0,0,0,0,0],
      [0,0,0,0,2],
      [3,0,0,0,0],
      [3,0,0,0,2]]

# material input
E = 32*10**9 # Pa
A = 0.1*0.1 # m
Ec = E

fcd = 20*10**6 # Pa
fck = 30*10**6 # Pa

#%% MODEL 2
"""
z = 1.5

# Shape 1
mb = [[0,0,0,0],
      [3,0,0,1],
      [3,0,z,2],
      [0,0,z,3]]

ma = [[0,0.01,0],
      [3,0.01,0],
      [3,0.01,z],
      [0,0.01,z]]


mp = [0.1,0.0005,0.1]



# load
# lp - load point [point_x, point_y, point_z, value, direction]

# lp = [[1,0,1,-960000,2],
#       [2,0,1,-960000,2]]

lp = [[1.5,0,0,-960000,2]]

# support
sp = [[0,0,0,0,0],
      [0,0,0,0,2],
      [3,0,0,0,0],
      [3,0,0,0,2]]

# material input
E = 32*10**9
A = 0.1*0.1
Ec = E

fcd = 30*10**6
"""
#%% ITERACE poznámky

"""
vnitřní síly - odebrat nejméně zatížené pruty(počet) 
"""
#%%% MODEL

result = generate_list_of_plane(mb, ma)
# plot_solid(mb, ma, 4,'black', '-',1, 'Shape model')

# generate mesh of points
x = generate_points_cube(0,3, 0.5)
y = generate_points_cube(0,2, 0.5)
z = generate_points_cube(0,1.5, 1)
cube = connect_points(x,y,z)

# generate points
points_inside = points_inside_planes(cube, mp, result, True, point_inside_shape)
# plot_points(points_inside)
bars = generate_bars_0(points_inside)
# plot_p_and_b(points_inside, bars, 'gray', '-',0.5, 'Shape model')

NODES = points_inside
# plot_p_and_b(NODES, bars,'black','-',0.1,'Ground truss')
#%% BOUNDARY CONDITIONS

# generate vector u
u = np.array([])
number = len(NODES)*len(NODES[0])
varname = 'u'
for i in range(number):
    exec(f"{varname}{i+1} = symbols('{varname}{i+1}')")
    exec(f"u = np.r_[u, {varname}{i+1}]")



    # supports - 2D, axis y = 1
axis = 1
for i in range(len(NODES)):
    u[i*3+axis] = 0
    
    # supports - other
u = load_point(sp, NODES, u)


# generate vector f
f = np.array([])
varname = 'f'
for i in range(len(u)):
    if type(u[i]) == int or type(u[i]) == float:
        exec(f"{varname}{i+1} = symbols('{varname}{i+1}')")
        exec(f"f = np.r_[f, {varname}{i+1}]")
    else:
        exec(f"{varname}{i+1} = symbols('{varname}{i+1}')")
        exec(f"{varname}{i+1} = float(0)") 
        exec(f"f = np.r_[f, {varname}{i+1}]")
        

# generate load

f = load_point(lp, NODES, f)

f = np.reshape(f,[len(f),1])
u = np.reshape(u,[len(u),1])

f_loop = copy.deepcopy(f)
u_loop = copy.deepcopy(u)

BARS = bars
#%% Plot

fig = plt.figure()
plt.axis('equal')  
plot_2d(NODES, BARS, 'gray', '-',0.5, 'Bars')
plt.title("Ground truss")

# Seznam všech globálních proměnných, které chcete uložit
variables_to_save = [NODES, BARS, f, u, f_loop, u_loop]

# Uložení proměnných do souboru
with open('01_example_model_mesh_00.pkl', 'wb') as f:
    dill.dump(variables_to_save, f)
    
    
import os

# Získání úplné cesty k aktuálnímu souboru
current_file_path = os.path.abspath(__file__)

# Výpis cesty
print("Cesta k aktuálnímu souboru:", current_file_path)

