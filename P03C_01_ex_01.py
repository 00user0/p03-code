# -*- coding: utf-8 -*-
"""
Created on Fri Nov  3 16:10:53 2023

@author: matth
"""

from STM_functions import *
from STM_function_solve import *
from STM_funkce_check_node_stability import *
import dill
from scipy.optimize import minimize, rosen, rosen_der

# Úplná cesta k souboru s uloženými proměnnými
# file_path = 'C:/Users/matth/Desktop/CVUT_od 09_2020/MG 23 LETNI/P04C/code/modely/wall_2_saved_variables_podpora_vpravo.pkl'
file_path = 'C:/Users/matth/Desktop/CVUT_od 09_2020/MG 23 LETNI/P04C/code/modely/01_example_model_mesh_00.pkl'

#%% initial material input
E = 32e9 # Pa
# A = 0.1*0.1 # m
# fck = 30*10**6 # Pa
# fcd = fck/1.5 # Pa



#%% Model
# Load variables of model
with open(file_path, 'rb') as f:
    loaded_variables = dill.load(f)

NODES = loaded_variables[0]
BARS = loaded_variables[1]
f = loaded_variables[2]
u = loaded_variables[3]
f_loop = loaded_variables[4]
u_loop = loaded_variables[5]

# f_BARS, BARS_pos, BARS_neg, BARS_no, f_s, u_s, u_BARS, u_pos, u_neg = solve_truss(NODES, BARS, u, f, E, A)


fig = plt.figure()
plt.axis('equal')  
plot_2d(NODES, BARS, 'gray', '-',0.5, 'Bars')
plt.title("Ground truss")

#%% Optimization

# generate vector t with unknown 
t = []


# define V
V = 1

# lenght of uknown t
n_unknown = len(BARS)

E = [30*10**6]*len(BARS)


# number = len(BARS)
# varname = 't'
# for i in range(number):
#     exec(f"{varname}{i+1} = symbols('{varname}{i+1}')")
#     exec(f"t.append({varname}{i+1})")

# a is index denote to generate
# a_K, a_K_solve, a_f_result, a_u_result = generate_ku_f(NODES, BARS, u, f, E, t)

# t = [1]*len(BARS)
# a_K, unkn = generate_ku_f(NODES, BARS, u, f, E, t)

# objective function
def objective_function_calc(t):
    force, displacement, stiffnes_matrix = solve_truss_optimization(NODES, BARS, u, f, E, t)
    force = force.flatten()
    return -np.dot(force, displacement)

def objective_function(t):
    return objective_function_calc(t)
    
# constraints 
def constraint_1_calc(t):
    force, displacement, stiffnes_matrix = solve_truss_optimization(NODES, BARS, u, f, E, t)
    return stiffnes_matrix @ displacement == force

def constraint_1(t):
    pass
    # return constraint_1_calc(t)
      
def constraint_2(t):
    return sum(t) - V

def constraint_3(t):
    return [x - 1e-6 for x in t]

# t = [1]*len(BARS)
# print(objective_function(t))


constraints = [{'type': 'ineq', 'fun': constraint_1},
                {'type': 'ineq', 'fun': constraint_2},
                {'type': 'ineq', 'fun': constraint_3}]

guess = [0.001]*n_unknown

result = minimize(objective_function, x0=guess, constraints=constraints)

print("Výsledek minimalizace:")
print("Optimalní hodnoty x:", result.x)
print("Minimální hodnota cílové funkce:", result.fun)