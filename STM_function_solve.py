# -*- coding: utf-8 -*-
"""
Created on Sat Feb 25 11:17:00 2023

@author: matth
"""
import numpy as np
import sympy as sp
from sympy import symbols, Matrix, solve
import math
import matplotlib
import matplotlib.pyplot as plt
from math import sqrt, floor, ceil
import random
import copy
from copy import deepcopy
# material properties

    # reinforcement
Es = 200e9 # Pa   
fyd = 435e6 # Pa
ms = 7850 #kg/m3

d_rf = [6, 8, 10, 12, 14, 16, 18, 20, 22, 25, 28, 32]
A_rf = []
for i in d_rf:
    A_rf.append(round(math.pi*i**2/4,2))
 
    
    # concrete
list_fck = [12, 16, 20, 25, 30, 35, 40, 45, 50]  

Ec = 32e9 # Pa
fck = 30e6 # Pa
fcd = fck/1.5 # Pa

    # deleted bar
E_del = 10**(-20)
#%% add bar properties and delete bars

"""
def plot_sup(list_supports):
     s1 = [0,0,0]
     s2 = [-0.5,0,-0.866]
     s2 = [0.5,0,-0.866]
     
     lines_support = []
"""
def bar_add_prop_constant(f_BARS, E):
    BARS_area = []
    BARS_modul = []
    for i in range(len(f_BARS)):
        accurancy = 5
        if f_BARS[i] > accurancy or f_BARS[i]< -accurancy:
            A = 0.01**2
            E = 10**(6)
            BARS_area.append(A)
            BARS_modul.append(E)
        
        if f_BARS[i] == 0:
            A = 0.001**2
            E = 10**(-6)
        BARS_area.append(A)
        BARS_modul.append(E)
        
    return BARS_area, BARS_modul

def bar_add_prop(f_BARS, Ec, fcd):
    BARS_area = []
    BARS_modul = []
    
    # design strenght of concrete strut
    v = 0.8
    s_rd_max = (1-fck*10**(-6)/250)*v*fcd
    
    for i in range(len(f_BARS)):
        if f_BARS[i] > 0:
            A = f_BARS[i]/fyd # or add minimal area

            # A = max(f_BARS[i]/fyd, max(f_BARS)/fyd/10) # or add minimal area
            E = Es
                    
        if f_BARS[i] < 0:
            
            # concrete unified area
            A = -min(f_BARS)/fcd
            
            # concrete non unified area
            # A = -f_BARS[i]/fcd
            E = Ec
            
            
        if f_BARS[i] == 0:
            A = 0.001**2
            E = 10**(-12)
        BARS_area.append(A)
        BARS_modul.append(E)
    return BARS_area, BARS_modul


def bar_add_prop_2(f_BARS, index_deleted):

    BARS_area = []
    BARS_modul = []
      
    for i in range(len(f_BARS)):
        
        
        # if i-bar is already deleted (if i == index j)
        if i in index_deleted:
            BARS_area.append(1e-15)
            BARS_modul.append(E_del)
            
            continue
        
        if f_BARS[i] > 0:
            A = f_BARS[i]/fyd
            E = Es
                    
        if f_BARS[i] < 0:
            
            # concrete unified area
            A = -min(f_BARS)/fcd
            
            # concrete non unified area
            # A = -f_BARS[i]/fcd
            E = Ec
            
        if f_BARS[i] == 0:
            A = 0.001**2
            E = 10**(-12)            

        BARS_area.append(A)
        BARS_modul.append(E)
        
    return BARS_area, BARS_modul


def delete_bars(number, f_BARS, BARS_modul, odd):
    # delete
    f = deepcopy(abs(f_BARS).tolist())
    list_min_index = []
    for i in range(number):
        index_min = f.index(min(f))
        list_min_index.append(index_min)
        # cancel newly found min value from f list
        f[index_min] = 10**9
               
        if i == (number-1) and odd == True: # ex. range(number=1) => only i=0 
                    # number - 1 = 1-1=0 equivalent i = 0
            if len(f_BARS) % 2 != 0 and number % 2 == 0:
                f_r = f_BARS[list_min_index[i-1]]
                f_l = f_BARS[list_min_index[i]]
                if  f_l < f_r*1.01 and f_l > f_r*0.99:
                    list_min_index = list_min_index[0:(number-1)]
    for i in list_min_index:
        BARS_modul[i] = 10**(-6)
        
    return BARS_modul

def delete_bars_0(number, f_BARS, BARS_modul, odd):
    # delete slightly
    # delete
    f = deepcopy(abs(f_BARS).tolist())
    list_min_index = []
    for i in range(number):
        index_min = f.index(min(f))
        list_min_index.append(index_min)
        # cancel newly found min value from f list
        f[index_min] = 10**9
               
        if i == (number-1) and odd == True: # ex. range(number=1) => only i=0 
                    # number - 1 = 1-1=0 equivalent i = 0
            if len(f_BARS) % 2 != 0 and number % 2 == 0:
                
                #
                f_r = f_BARS[list_min_index[i-1]]
                f_l = f_BARS[list_min_index[i]]
                if  f_l < f_r*1.01 and f_l > f_r*0.99:
                    list_min_index = list_min_index[0:(number-1)]
    for i in list_min_index:
        BARS_modul[i] = max(10e-6,BARS_modul[i]/1000)
        
    return BARS_modul


# delete_bars_2 - this function remember indexes of deleted bars

def delete_bars_2(number, f_BARS, BARS_modul, index_deleted):
    
    # delete
    f = deepcopy(abs(f_BARS).tolist())
    for i in range(number):
        
        # repeat finding minimal force
        for j in range(len(f)):            
                index_min = f.index(min(f))
                # free up space in f list for new min value
                f[index_min] = max(f)
                if index_min not in index_deleted:
                    # add index to list - deleted bars index             
                    index_deleted.append(index_min)
                    break

               
    for i in index_deleted:
        BARS_modul[i] = E_del
        
    return BARS_modul, index_deleted

def delete_bars_2_1(number, f_BARS, BARS_modul, index_deleted):
    
    # delete
    f = deepcopy(abs(f_BARS).tolist())
    
    # new deleted bars
    new_index = []
    
    for i in range(number):
        
        # repeat finding minimal force
        for j in range(len(f)):            
                index_min = f.index(min(f))
                # free up space in f list for new min value
                f[index_min] = max(f)
                if index_min not in index_deleted:
                    # add index to list - deleted bars index             
                    new_index.append(index_min)
                    break

    index_deleted = index_deleted + new_index
         
    for i in index_deleted:
        BARS_modul[i] = E_del
        
    return BARS_modul, index_deleted, new_index


# useful for deleting ties only
def delete_bars_2_2(number, f_BARS, BARS_modul, index_deleted):
    
    # delete
    f = deepcopy(f_BARS.tolist())
    
    
    # to delete only ties change negative forces to high positive 
    f_max = max(f)
    for i in range(len(f)):
        if f[i] < 0:
            f[i] = f_max*1.1
    
    # new deleted bars
    new_index = []
    
    for i in range(number):
        
        # repeat finding minimal force
        for j in range(len(f)):            
            index_min = f.index(min(f))
            # free up space in f list for new min value
            f[index_min] = max(f)
            if index_min not in index_deleted:
                # add index to list - deleted bars index             
                new_index.append(index_min)
                break

    index_deleted = index_deleted + new_index
         
    for i in index_deleted:
        BARS_modul[i] = E_del
        
    return BARS_modul, index_deleted, new_index

# useful for deleting ties only with specific F_del
def delete_bars_2_3(F_del, f_BARS, BARS_modul, index_deleted):
    
    # delete
    f = deepcopy(f_BARS.tolist())
        
    # new deleted bars
    new_index = []
    
    # finding minimal area force
    for j in range(len(f)):    
        if f[j] >= 0 and f[j] <= F_del:
            index_min = j
            # free up space in f list for new min value
            f[index_min] = max(f)
            if index_min not in index_deleted:
                # add index to list - deleted bars index             
                new_index.append(index_min)

    index_deleted = index_deleted + new_index
         
    for i in index_deleted:
        BARS_modul[i] = E_del
        
    return BARS_modul, index_deleted, new_index
        
def bars_lenght(NODES, BARS):
    bars_lenght = []     
    for i in range(len(BARS)):
        # for loop - choose node in particular bar one by one
        # calculate vector and lenght
        # V - local axis vectors of bars
        vx = NODES[BARS[i][1]][0]-NODES[BARS[i][0]][0]
        vy =  NODES[BARS[i][1]][1]-NODES[BARS[i][0]][1]
        vz = NODES[BARS[i][1]][2]-NODES[BARS[i][0]][2]
    
    
        V = np.array([[vx],
                  [vy],
                  [vz]
                  ])
    
        L = np.linalg.norm(V)
        bars_lenght.append(L)        
    return bars_lenght
    

#%% STIFFNESS MATRIX

def solve_truss(NODES, BARS, u, f, E, A):
    modul = 0
    if type(E) == list:
        modul = copy.deepcopy(E)
        area = copy.deepcopy(A)
    
    # create empty stiffness matrix with correct size
    K = np.zeros((len(u),len(u)))
    
    # for loop - choose bar one by one
    for i in range(len(BARS)):
        # for loop - choose node in particular bar one by one
        # calculate vector and lenght
        # V - local axis vectors of bars
        vx = NODES[BARS[i][1]][0]-NODES[BARS[i][0]][0]
        vy =  NODES[BARS[i][1]][1]-NODES[BARS[i][0]][1]
        vz = NODES[BARS[i][1]][2]-NODES[BARS[i][0]][2]
    
    
        V = np.array([[vx],
                  [vy],
                  [vz]
                  ])
    
        L = np.linalg.norm(V)
        cx = vx/L
        cy = vy/L
        cz = vz/L       
        
        if type(modul) == list:
            E = modul[i]
            A = area[i]
            
            
            # print(E)
            # print(A)
            # print(E*A)
            
        K_ge_1 = (E*A)/L * np.array([[cx**2, cx*cy, cx*cz, -cx**2, -cx*cy, -cx*cz],
                         [cx*cy, cy**2, cy*cz, -cx*cy, -cy**2, -cy*cz],
                         [cx*cz, cy*cz, cz**2, -cx*cz, -cy*cz, -cz**2],
                         ])
        K_ge = np.concatenate((K_ge_1,-K_ge_1))
                
        
        # add bar stiffnes matrix to global matrix
        for j in range(len(K_ge_1)):
            for l in range(len(K_ge_1)):
                # add stiffnes of 1. node - top left
                K [(BARS[i][0])*3 + j] [(BARS[i][0])*3+l] += float(K_ge_1[j][l])
    
                # add stiffnes of 1. 2. nodes - top right
                K [BARS[i][0]*3 + j] [BARS[i][1]*3+l] += float(K_ge_1[j][l+3])
                # add stiffnes of 2. 1. node - down left
                K [BARS[i][1]*3 + j] [BARS[i][0]*3+l] += float(-K_ge_1[j][l])
                # add stiffnes of 2. nodes - down right
                K [BARS[i][1]*3 + j] [BARS[i][1]*3+l] += float(-K_ge_1[j][l+3])
    
    
    #%% SOLVE
    
    # vytvoreni kopii vektoru a matic pro preskupeni neznamych do jednoho vektoru
    K_solve = copy.deepcopy(K)
    u_solve = copy.deepcopy(u)
    f_solve = copy.deepcopy(f)
    
    ## presun f=? do vektoru deformaci
    
    for i in range(len(f_solve)):
        if type(f_solve[i][0]) != int and type(f_solve[i][0]) != float:
            
            #pridani f[i] do vektoru deformaci
            u_solve = np.append(u_solve,[f_solve[i][0]])
            
            #vytvoreni pole, ktere kompenzuje f ve vektoru deformaci
            comp_vector = np.zeros((len(K_solve),1))
            K_solve=np.append(K_solve,comp_vector,axis=1)
            # kompenzace v matici tuhosti       
            K_solve[i][-1]=-1               
    
    
    ## rewrite elements f_i in vector f_solve for zero
    for i in range(len(f_solve)):
        # pokud je f_i int nebo float ->
        if type(f_solve[i][0]) != int and type(f_solve[i][0]) != float:
            f_solve[i][0] = 0
    
    # presun u = zname (podpory) do vektoru sil
    for i in range(len(K)):   
        # pokud je u_solve[i] int nebo float ->
        if type(u_solve[i]) == int or type(u_solve[i]) == float:      
            #odecteni ke vsem radkum vektoru f ui-nasobek
            for j in range(len(K)):
                f_solve[j][0]+=-K[i][j]*u[i][0]
     
    i_elem = 0
    while i_elem < len(u_solve):
        # if ui is int or float -> delete column or row in K or u
        if type(u_solve[i_elem]) == int or type(u_solve[i_elem]) == float:
            K_solve = np.delete(K_solve,i_elem,axis=1)
            u_solve = np.delete(u_solve,i_elem,axis=0)
        else:
            i_elem += 1
        
    
    f_solve_float = np.zeros((len(f_solve),1))
    for i in range(len(f_solve)):
        f_solve_float[i][0]=float(f_solve[i][0])
    
    
    if np.linalg.det(K_solve) !=0:
        solved_vector = np.linalg.solve(K_solve, f_solve_float)
    # print(solved_vector)
    else:
        return  None
    
        
    
    # aktualisace matice, vektorů - řešení

    for i in range(len(f_solve_float)):
        for j in range(len(f_solve_float)):
            if u_solve[i] == u[j][0]:
                u[j][0] = solved_vector[i][0]
                continue
            if u_solve[i] == f[j][0]:
                f[j][0] =  solved_vector[i][0]

    #%% Internal forces
    
    f_NODES = np.reshape(u,[len(NODES),3])
    f_BARS = np.empty((1,0))
    u_BARS = np.empty((1,0))
    # for loop - choose bar one by one
    for i in range(len(BARS)):
        
        if type(modul) == list:

            E = modul[i]
            A = area[i]

        vx = NODES[BARS[i][1]][0]-NODES[BARS[i][0]][0]
        vy =  NODES[BARS[i][1]][1]-NODES[BARS[i][0]][1]
        vz = NODES[BARS[i][1]][2]-NODES[BARS[i][0]][2]
     
        V = np.array([[vx],
                      [vy],
                      [vz]])
        L = np.linalg.norm(V)
        dlx = (f_NODES[BARS[i][1]][0]-f_NODES[BARS[i][0]][0])*vx/L
        dly = (f_NODES[BARS[i][1]][1]-f_NODES[BARS[i][0]][1])*vy/L
        dlz = (f_NODES[BARS[i][1]][2]-f_NODES[BARS[i][0]][2])*vz/L
    
        dl = (dlx+dly+dlz)/L
        
        # linear approach
        f_BARS = np.append(f_BARS, E*A*dl)
        u_BARS = np.append(u_BARS, dl)        
        
        
        # # bilinear diagram for steel
        # if dl>0:
        #     f_BARS = np.append(f_BARS, min(fyd*A, E*A*dl))

        # else:
        #     f_BARS = np.append(f_BARS, E*A*dl) 
        
        # u_BARS = np.append(u_BARS, dl)
        
        ## non-linear approach
        """
        if dl>0:
            f_BARS = np.append(f_BARS, min(fyd*A, round(E*A*dl,2)))
            # f_BARS = np.append(f_BARS, round(E*A*dl,2))

        else:
            # f_BARS = np.append(f_BARS, min(fcd*A, round(E*A*dl,2)))
            f_BARS = np.append(f_BARS, round(E*A*dl,2))


        u_BARS = np.append(u_BARS, dl)        

        ## bilinear diagram for steel and concrete

        u_BARS = np.append(u_BARS, dl)
        if dl>0:
            esy = fyd/Es
            if dl <= esy:
                f_BARS = np.append(f_BARS, round(E*A*dl,2))
            if dl > esy:
                f_BARS = np.append(f_BARS, round(fyd*A,2))
        else:
            ecu3 = 3.5e-3
            ec3 = 1.75e-3
            Ecb = fcd/ec3 # E bilinear
            if -dl <= ec3:
                f_BARS = np.append(f_BARS, round(Ecb*A*dl,2))
            if -dl > ec3:
                f_BARS = np.append(f_BARS, round(-fcd*A,2))
            # f_BARS = np.append(f_BARS, round(E*A*dl,2))
"""
            
    # print(f_BARS)
    BARS_pos = np.empty((0,3)).astype(int)
    BARS_neg = np.empty((0,3)).astype(int)
    BARS_no = np.empty((0,3)).astype(int)
    u_pos = np.empty((0,1))
    u_neg = np.empty((0,1))
    
    # create bars with +/- axial force
    for i in range(len(BARS)):
        
        bar_tuple = int(BARS[i][0]), int(BARS[i][1]), int(f_BARS[i])
        
        if f_BARS[i] > 0:
            BARS_pos = np.append(BARS_pos, np.array([bar_tuple]), axis = 0)
            u_pos = np.append(u_pos, u_BARS[i])
        if f_BARS[i] < 0:
            BARS_neg = np.append(BARS_neg, np.array([bar_tuple]), axis = 0)
            u_neg = np.append(u_neg, u_BARS[i])
        else:
            BARS_no = np.append(BARS_no, np.array([bar_tuple]), axis = 0)
    
    # print("Vnitřní síly v jednotlivých prutech [kN]: ", f_BARS/1000)
    return f_BARS, BARS_pos, BARS_neg, BARS_no, f, u, u_BARS, u_pos, u_neg


#%% STIFFNESS MATRIX 22
# *****************************************
# *****************************************
# *****************************************
# *****************************************
# *****************************************
# *****************************************
# *****************************************
# *****************************************
# *****************************************
# *****************************************
# *****************************************
# *****************************************
# *****************************************
# *****************************************
# *****************************************
# *****************************************
# *****************************************
# *****************************************
# *****************************************
# *****************************************
# *****************************************
# *****************************************
# *****************************************
# *****************************************


def check_stability_of_node(NODES, BARS, u, f, E, A):
    modul = 0
    if type(E) == list:
        modul = copy.deepcopy(E)
        area = copy.deepcopy(A)
    
    # create empty stiffness matrix with correct size
    K = np.zeros((len(u),len(u)))
    
    # for loop - choose bar one by one
    for i in range(len(BARS)):
        # for loop - choose node in particular bar one by one
        # calculate vector and lenght
        # V - local axis vectors of bars
        vx = NODES[BARS[i][1]][0]-NODES[BARS[i][0]][0]
        vy =  NODES[BARS[i][1]][1]-NODES[BARS[i][0]][1]
        vz = NODES[BARS[i][1]][2]-NODES[BARS[i][0]][2]
    
    
        V = np.array([[vx],
                  [vy],
                  [vz]
                  ])
    
        L = np.linalg.norm(V)
        cx = vx/L
        cy = vy/L
        cz = vz/L       
        
        if type(modul) == list:
            E = modul[i]
            A = area[i]
            
            
            # print(E)
            # print(A)
            # print(E*A)
            
        K_ge_1 = (E*A)/L * np.array([[cx**2, cx*cy, cx*cz, -cx**2, -cx*cy, -cx*cz],
                         [cx*cy, cy**2, cy*cz, -cx*cy, -cy**2, -cy*cz],
                         [cx*cz, cy*cz, cz**2, -cx*cz, -cy*cz, -cz**2],
                         ])
        K_ge = np.concatenate((K_ge_1,-K_ge_1))
                
        
        # add bar stiffnes matrix to global matrix
        for j in range(len(K_ge_1)):
            for l in range(len(K_ge_1)):
                # add stiffnes of 1. node - top left
                K [(BARS[i][0])*3 + j] [(BARS[i][0])*3+l] += float(K_ge_1[j][l])
    
                # add stiffnes of 1. 2. nodes - top right
                K [BARS[i][0]*3 + j] [BARS[i][1]*3+l] += float(K_ge_1[j][l+3])
                # add stiffnes of 2. 1. node - down left
                K [BARS[i][1]*3 + j] [BARS[i][0]*3+l] += float(-K_ge_1[j][l])
                # add stiffnes of 2. nodes - down right
                K [BARS[i][1]*3 + j] [BARS[i][1]*3+l] += float(-K_ge_1[j][l+3])
    

    
    # vytvoreni kopii vektoru a matic pro preskupeni neznamych do jednoho vektoru
    K_solve = copy.deepcopy(K)
    u_solve = copy.deepcopy(u)
    f_solve = copy.deepcopy(f)
    
    ## presun f=? do vektoru deformaci
    
    for i in range(len(f_solve)):
        if type(f_solve[i][0]) != int and type(f_solve[i][0]) != float:
            
            #pridani f[i] do vektoru deformaci
            u_solve = np.append(u_solve,[f_solve[i][0]])
            
            #vytvoreni pole, ktere kompenzuje f ve vektoru deformaci
            comp_vector = np.zeros((len(K_solve),1))
            K_solve=np.append(K_solve,comp_vector,axis=1)
            # kompenzace v matici tuhosti       
            K_solve[i][-1]=-1               
    
    
    ## rewrite elements f_i in vector f_solve for zero
    for i in range(len(f_solve)):
        # pokud je f_i int nebo float ->
        if type(f_solve[i][0]) != int and type(f_solve[i][0]) != float:
            f_solve[i][0] = 0
    
    # presun u = zname (podpory) do vektoru sil
    for i in range(len(K)):   
        # pokud je u_solve[i] int nebo float ->
        if type(u_solve[i]) == int or type(u_solve[i]) == float:      
            #odecteni ke vsem radkum vektoru f ui-nasobek
            for j in range(len(K)):
                f_solve[j][0]+=-K[i][j]*u[i][0]
     
    i_elem = 0
    while i_elem < len(u_solve):
        # if ui is int or float -> delete column or row in K or u
        if type(u_solve[i_elem]) == int or type(u_solve[i_elem]) == float:
            K_solve = np.delete(K_solve,i_elem,axis=1)
            u_solve = np.delete(u_solve,i_elem,axis=0)
        else:
            i_elem += 1
        
    
    f_solve_float = np.zeros((len(f_solve),1))
    for i in range(len(f_solve)):
        f_solve_float[i][0]=float(f_solve[i][0])
    
    
    if np.linalg.det(K_solve) !=0:
        solved_vector = 1
    
    else:
        solved_vector = "unstable"
    
    return solved_vector

#%%

def solve_truss_2(NODES, BARS, u, f, E, A):
    # create empty stiffness matrix with correct size
    K = np.zeros((len(u),len(u)))
    
    # for loop - choose bar one by one
    for i in range(len(BARS)):
        # for loop - choose node in particular bar one by one
        # calculate vector and lenght
        # V - local axis vectors of bars
        vx = NODES[BARS[i][1]][0]-NODES[BARS[i][0]][0]
        vy =  NODES[BARS[i][1]][1]-NODES[BARS[i][0]][1]
        vz = NODES[BARS[i][1]][2]-NODES[BARS[i][0]][2]
    
    
        V = np.array([[vx],
                  [vy],
                  [vz]
                  ])
    
        L = np.linalg.norm(V)
        cx = vx/L
        cy = vy/L
        cz = vz/L       
    
        if type(E) == list:
            E = E[i]
            A = A[i]
        K_ge_1 = (E*A)/L * np.array([[cx**2, cx*cy, cx*cz, -cx**2, -cx*cy, -cx*cz],
                         [cx*cy, cy**2, cy*cz, -cx*cy, -cy**2, -cy*cz],
                         [cx*cz, cy*cz, cz**2, -cx*cz, -cy*cz, -cz**2],
                         ])
        K_ge = np.concatenate((K_ge_1,-K_ge_1))
        
        # add bar stiffnes matrix to global matrix
        for j in range(len(K_ge_1)):
            for l in range(len(K_ge_1)):
                # add stiffnes of 1. node - top left
                K [(BARS[i][0])*3 + j] [(BARS[i][0])*3+l] += float(K_ge_1[j][l])
    
                # add stiffnes of 1. 2. nodes - top right
                K [BARS[i][0]*3 + j] [BARS[i][1]*3+l] += float(K_ge_1[j][l+3])
                # add stiffnes of 2. 1. node - down left
                K [BARS[i][1]*3 + j] [BARS[i][0]*3+l] += float(-K_ge_1[j][l])
                # add stiffnes of 2. nodes - down right
                K [BARS[i][1]*3 + j] [BARS[i][1]*3+l] += float(-K_ge_1[j][l+3])
    
    
    
    #%% SOLVE
    
    # vytvoreni kopii vektoru a matic pro preskupeni neznamych do jednoho vektoru
    K_solve = K.copy()
    u_solve = u.copy()
    f_solve = f.copy()
    
    ## presun f=? do vektoru deformaci
    
    for i in range(len(f_solve)):
        if type(f_solve[i][0]) != int and type(f_solve[i][0]) != float:
            
            #pridani f[i] do vektoru deformaci
            u_solve = np.append(u_solve,[f_solve[i][0]])
            
            #vytvoreni pole, ktere kompenzuje f ve vektoru deformaci
            comp_vector = np.zeros((len(K_solve),1))
            K_solve=np.append(K_solve,comp_vector,axis=1)
            # kompenzace v matici tuhosti       
            K_solve[i][-1]=-1               
    
    
    ## rewrite elements f_i in vector f_solve for zero
    for i in range(len(f_solve)):
        # pokud je f_i int nebo float ->
        if type(f_solve[i][0]) != int and type(f_solve[i][0]) != float:
            f_solve[i][0] = 0
    
    # presun u = zname (podpory) do vektoru sil
    for i in range(len(K)):   
        # pokud je u_solve[i] int nebo float ->
        if type(u_solve[i]) == int or type(u_solve[i]) == float:      
            #odecteni ke vsem radkum vektoru f ui-nasobek
            for j in range(len(K)):
                f_solve[j][0]+=-K[i][j]*u[i][0]
     
    i_elem = 0
    while i_elem < len(u_solve):
        # if ui is int or float -> delete column or row in K or u
        if type(u_solve[i_elem]) == int or type(u_solve[i_elem]) == float:
            K_solve = np.delete(K_solve,i_elem,axis=1)
            u_solve = np.delete(u_solve,i_elem,axis=0)
        else:
            i_elem += 1
        
    
    f_solve_float = np.zeros((len(f_solve),1))
    for i in range(len(f_solve)):
        f_solve_float[i][0]=float(f_solve[i][0])
    
    print(str(len(K_solve[0])))
    
    return K_solve
"""
    solved_vector = np.linalg.solve(K_solve, f_solve_float)
    # print(solved_vector)
    
    
    # aktualisace matice, vektorů - řešení
    for i in range(len(f_solve_float)):
        for j in range(len(f_solve_float)):
            if u_solve[i] == u[j][0]:
                u[j][0] = solved_vector[i][0]
                continue
            if u_solve[i] == f[j][0]:
                f[j][0] =  solved_vector[i][0]
"""

#%% generate ku = f symbolically
def generate_ku_f(NODES, BARS, u, f, E, t):
    modul = 0
    if type(E) == list:
        modul = copy.deepcopy(E)
        volume = copy.deepcopy(t)
    
    # create empty stiffness matrix with correct size
    K = np.zeros((len(u),len(u)))
    
    # for loop - choose bar one by one
    for i in range(len(BARS)):
        # for loop - choose node in particular bar one by one
        # calculate vector and lenght
        # V - local axis vectors of bars
        vx = NODES[BARS[i][1]][0]-NODES[BARS[i][0]][0]
        vy =  NODES[BARS[i][1]][1]-NODES[BARS[i][0]][1]
        vz = NODES[BARS[i][1]][2]-NODES[BARS[i][0]][2]
    
    
        V = np.array([[vx],
                  [vy],
                  [vz]
                  ])
    
        L = np.linalg.norm(V)
        cx = vx/L
        cy = vy/L
        cz = vz/L       
        
        if type(E) == list:
            E = modul[i]
            
            
            # print(E)
            # print(A)
            # print(E*A)
            
        K_ge_1 = (E*t[i])/(L**2) * Matrix([[cx**2, cx*cy, cx*cz, -cx**2, -cx*cy, -cx*cz],
                         [cx*cy, cy**2, cy*cz, -cx*cy, -cy**2, -cy*cz],
                         [cx*cz, cy*cz, cz**2, -cx*cz, -cy*cz, -cz**2]
                         ])
        K_ge = np.concatenate((K_ge_1,-K_ge_1))
                
        
        # add bar stiffnes matrix to global matrix
        for j in range(K_ge_1.rows):
            for l in range(K_ge_1.rows):
                K = Matrix(K)
                # add stiffnes of 1. node - top left
                
                K [(BARS[i][0])*3 + j,(BARS[i][0])*3+l] += K_ge_1[j,l]
                
                # add stiffnes of 1. 2. nodes - top right
                K [BARS[i][0]*3 + j,BARS[i][1]*3+l] += K_ge_1[j,l+3]
                # add stiffnes of 2. 1. node - down left
                K [BARS[i][1]*3 + j,BARS[i][0]*3+l] += -K_ge_1[j,l]
                # add stiffnes of 2. nodes - down right
                K [BARS[i][1]*3 + j,BARS[i][1]*3+l] += -K_ge_1[j,l+3]
    
    
    unknown = f.flatten().tolist()+ u.flatten().tolist()
    unknown_2 = []

    for i in unknown:
        if type(i) != float and type (i) != int:
            unknown_2.append(i)
    print(unknown_2)
    
    #########################
    ########################
    
    # vytvoreni kopii vektoru a matic pro preskupeni neznamych do jednoho vektoru
    K_solve = copy.deepcopy(K)
    u_solve = copy.deepcopy(u)
    f_solve = copy.deepcopy(f)
    
  
    # print(u)
    # print(f)
    
    ## presun f=? do vektoru deformaci
    
    for i in range(len(f_solve)):
        if type(f_solve[i][0]) != int and type(f_solve[i][0]) != float:
            
            #pridani f[i] do vektoru deformaci
            u_solve = np.append(u_solve,[f_solve[i][0]])
            
            #vytvoreni pole, ktere kompenzuje f ve vektoru deformaci
            comp_vector = sp.zeros(K_solve.rows,1)
            K_solve = K_solve.row_join(comp_vector)
            # kompenzace v matici tuhosti       
            K_solve[i,-1]=-1               
    
    ## rewrite elements f_i in vector f_solve to zero
    for i in range(len(f_solve)):
        # pokud je f_i int nebo float ->
        if type(f_solve[i][0]) != int and type(f_solve[i][0]) != float:
            f_solve[i][0] = 0
    
    # presun u = zname (podpory) do vektoru sil
    for i in range(K.rows):   
        # pokud je u_solve[i] int nebo float ->
        if type(u_solve[i]) == int or type(u_solve[i]) == float:      
            #odecteni ke vsem radkum vektoru f ui-nasobek
            for j in range(K.rows):
                f_solve[j][0]+=-K[i,j]*u[i][0]
     
    i_elem = 0
    while i_elem < len(u_solve):
        # if ui is int or float -> delete column in K or row in u
        if type(u_solve[i_elem]) == int or type(u_solve[i_elem]) == float:
            
            K_solve.col_del(i_elem)
            print(K_solve.shape)
            u_solve = np.delete(u_solve,i_elem,axis=0)
        else:
            i_elem += 1
    
    f_solve_float = np.zeros((len(f_solve),1))
    for i in range(len(f_solve)):
        f_solve_float[i][0]=float(f_solve[i][0])
    

###################################

    # equation = K_solve * u_solve - f_solve_float
    # # solution = solve(equation, (unknown_2))
    
    # f_solve_float = sp.Matrix(f_solve_float)
    # solution = sp.linsolve((K_solve, f_solve_float), [unknown_2])
    
    # 3)
    f_solve_float  = sp.Matrix(f_solve_float.tolist())
    u_solve = sp.Matrix(u_solve)
    print(type(f_solve_float))
    dot_Ku = K_solve @ u_solve
    solution = sp.linsolve((dot_Ku, f_solve_float), [unknown_2])

    # print(dot_Ku)
    
    # waste
    """
        
    # vytvoreni kopii vektoru a matic pro preskupeni neznamych do jednoho vektoru
    K_solve = copy.deepcopy(K)
    u_solve = copy.deepcopy(u)
    f_solve = copy.deepcopy(f)
    
    
    # print(u)
    # print(f)
    
    ## presun f=? do vektoru deformaci
    
    for i in range(len(f_solve)):
        if type(f_solve[i][0]) != int and type(f_solve[i][0]) != float:
            
            #pridani f[i] do vektoru deformaci
            u_solve = np.append(u_solve,[f_solve[i][0]])
            
            #vytvoreni pole, ktere kompenzuje f ve vektoru deformaci
            comp_vector = np.zeros((len(K_solve),1))
            K_solve=np.append(K_solve,comp_vector,axis=1)
            # kompenzace v matici tuhosti       
            K_solve[i][-1]=-1               
    
    
    ## rewrite elements f_i in vector f_solve for zero
    for i in range(len(f_solve)):
        # pokud je f_i int nebo float ->
        if type(f_solve[i][0]) != int and type(f_solve[i][0]) != float:
            f_solve[i][0] = 0
    
    # presun u = zname (podpory) do vektoru sil
    for i in range(len(K)):   
        # pokud je u_solve[i] int nebo float ->
        if type(u_solve[i]) == int or type(u_solve[i]) == float:      
            #odecteni ke vsem radkum vektoru f ui-nasobek
            for j in range(len(K)):
                f_solve[j][0]+=-K[i][j]*u[i][0]
     
    i_elem = 0
    while i_elem < len(u_solve):
        # if ui is int or float -> delete column or row in K or u
        if type(u_solve[i_elem]) == int or type(u_solve[i_elem]) == float:
            K_solve = np.delete(K_solve,i_elem,axis=1)
            u_solve = np.delete(u_solve,i_elem,axis=0)
        else:
            i_elem += 1
        
    
    f_solve_float = np.zeros((len(f_solve),1))
    for i in range(len(f_solve)):
        f_solve_float[i][0]=float(f_solve[i][0])
    
    
    solved_vector = np.linalg.solve(K_solve, f_solve_float)    
    
    # aktualisace matice, vektorů - řešení
    u_result = copy.deepcopy(u)
    f_result = copy.deepcopy(f)
    for i in range(len(f_solve_float)):
        for j in range(len(f_solve_float)):
            if u_solve[i] == u[j][0]:
                u_result[j][0] = solved_vector[i][0]
                continue
            if u_solve[i] == f[j][0]:
                f_result[j][0] =  solved_vector[i][0]
    
    return K, K_solve, f_result, u_result
    """
    return K, solution
#%% function
# t_v - volume
def solve_truss_optimization(NODES, BARS, u, f, E, t):
    modul = 0
    if type(E) == list:
        modul = copy.deepcopy(E)
        volume = copy.deepcopy(t)
    
    # create empty stiffness matrix with correct size
    K = np.zeros((len(u),len(u)))
    
    # for loop - choose bar one by one
    for i in range(len(BARS)):
        # for loop - choose node in particular bar one by one
        # calculate vector and lenght
        # V - local axis vectors of bars
        vx = NODES[BARS[i][1]][0]-NODES[BARS[i][0]][0]
        vy =  NODES[BARS[i][1]][1]-NODES[BARS[i][0]][1]
        vz = NODES[BARS[i][1]][2]-NODES[BARS[i][0]][2]
    
    
        V = np.array([[vx],
                  [vy],
                  [vz]
                  ])
    
        L = np.linalg.norm(V)
        cx = vx/L
        cy = vy/L
        cz = vz/L       
        
        if type(E) == list:
            E = modul[i]
            ti = volume[i]
            
            
            # print(E)
            # print(A)
            # print(E*A)
            
        K_ge_1 = (E*t[i])/(L**2) * np.array([[cx**2, cx*cy, cx*cz, -cx**2, -cx*cy, -cx*cz],
                         [cx*cy, cy**2, cy*cz, -cx*cy, -cy**2, -cy*cz],
                         [cx*cz, cy*cz, cz**2, -cx*cz, -cy*cz, -cz**2],
                         ])
        K_ge = np.concatenate((K_ge_1,-K_ge_1))
                
        
        # add bar stiffnes matrix to global matrix
        for j in range(len(K_ge_1)):
            for l in range(len(K_ge_1)):
                # add stiffnes of 1. node - top left
                K [(BARS[i][0])*3 + j] [(BARS[i][0])*3+l] += float(K_ge_1[j][l])
    
                # add stiffnes of 1. 2. nodes - top right
                K [BARS[i][0]*3 + j] [BARS[i][1]*3+l] += float(K_ge_1[j][l+3])
                # add stiffnes of 2. 1. node - down left
                K [BARS[i][1]*3 + j] [BARS[i][0]*3+l] += float(-K_ge_1[j][l])
                # add stiffnes of 2. nodes - down right
                K [BARS[i][1]*3 + j] [BARS[i][1]*3+l] += float(-K_ge_1[j][l+3])
    
    
    #%% SOLVE
    
    # vytvoreni kopii vektoru a matic pro preskupeni neznamych do jednoho vektoru
    K_solve = copy.deepcopy(K)
    u_solve = copy.deepcopy(u)
    f_solve = copy.deepcopy(f)
    
    
    # print(u)
    # print(f)
    
    ## presun f=? do vektoru deformaci
    
    for i in range(len(f_solve)):
        if type(f_solve[i][0]) != int and type(f_solve[i][0]) != float:
            
            #pridani f[i] do vektoru deformaci
            u_solve = np.append(u_solve,[f_solve[i][0]])
            
            #vytvoreni pole, ktere kompenzuje f ve vektoru deformaci
            comp_vector = np.zeros((len(K_solve),1))
            K_solve=np.append(K_solve,comp_vector,axis=1)
            # kompenzace v matici tuhosti       
            K_solve[i][-1]=-1               
    
    
    ## rewrite elements f_i in vector f_solve for zero
    for i in range(len(f_solve)):
        # pokud je f_i int nebo float ->
        if type(f_solve[i][0]) != int and type(f_solve[i][0]) != float:
            f_solve[i][0] = 0
    
    # presun u = zname (podpory) do vektoru sil
    for i in range(len(K)):   
        # pokud je u_solve[i] int nebo float ->
        if type(u_solve[i]) == int or type(u_solve[i]) == float:      
            #odecteni ke vsem radkum vektoru f ui-nasobek
            for j in range(len(K)):
                f_solve[j][0]+=-K[i][j]*u[i][0]
     
    i_elem = 0
    while i_elem < len(u_solve):
        # if ui is int or float -> delete column or row in K or u
        if type(u_solve[i_elem]) == int or type(u_solve[i_elem]) == float:
            K_solve = np.delete(K_solve,i_elem,axis=1)
            u_solve = np.delete(u_solve,i_elem,axis=0)
        else:
            i_elem += 1
        
    
    f_solve_float = np.zeros((len(f_solve),1))
    for i in range(len(f_solve)):
        f_solve_float[i][0]=float(f_solve[i][0])
    
    print(K_solve.shape)
    print(t)
    if np.linalg.det(K_solve) !=0:
        solved_vector = np.linalg.solve(K_solve, f_solve_float)
    # print(solved_vector)
    else:
        return  None
    
        
    
    # aktualisace matice, vektorů - řešení
    u_result = copy.deepcopy(u)
    f_result = copy.deepcopy(f)
    for i in range(len(f_solve_float)):
        for j in range(len(f_solve_float)):
            if u_solve[i] == u[j][0]:
                u_result[j][0] = solved_vector[i][0]
                continue
            if u_solve[i] == f[j][0]:
                f_result[j][0] =  solved_vector[i][0]

    #%% Internal forces
    
    f_NODES = np.reshape(u,[len(NODES),3])
    f_BARS = np.empty((1,0))
    u_BARS = np.empty((1,0))
    # for loop - choose bar one by one
    for i in range(len(BARS)):
        
        if type(modul) == list:

            E = modul[i]
            ti = volume[i]

        vx = NODES[BARS[i][1]][0]-NODES[BARS[i][0]][0]
        vy =  NODES[BARS[i][1]][1]-NODES[BARS[i][0]][1]
        vz = NODES[BARS[i][1]][2]-NODES[BARS[i][0]][2]
     
        V = np.array([[vx],
                      [vy],
                      [vz]])
        L = np.linalg.norm(V)
        dlx = (f_NODES[BARS[i][1]][0]-f_NODES[BARS[i][0]][0])*vx/L
        dly = (f_NODES[BARS[i][1]][1]-f_NODES[BARS[i][0]][1])*vy/L
        dlz = (f_NODES[BARS[i][1]][2]-f_NODES[BARS[i][0]][2])*vz/L
    
        dl = (dlx+dly+dlz)/L
        
        # linear approach
        f_BARS = np.append(f_BARS, E*t[i]/L*dl)
        u_BARS = np.append(u_BARS, dl)        
        
    
    # print("Vnitřní síly v jednotlivých prutech [kN]: ", f_BARS/1000)
    return f_result, u_result, K